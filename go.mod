module gitee.com/tfcolin/ftbt

go 1.20

require gitee.com/tfcolin/dsg v1.3.1-0.20230202085651-4dd3f39650e9

require (
	gitee.com/tfcolin/goncurses v0.0.0-20230720065243-ebe86618cac0
	github.com/rthornton128/goncurses v0.0.0-20230211155340-24ae0ddac304 // indirect
)
